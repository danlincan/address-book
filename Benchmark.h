#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <iostream>
#include <time.h>
#include <fstream>
#include <string>

class Benchmark
{
    timespec start, end;
    timespec timeDiff;
public:
    void Start();
    void Stop();
    void printResult(std::ostream *out);
};

#endif // BENCHMARK_H
