#ifndef CONTACT_H
#define CONTACT_H

#include <iostream>
#include <string>

using namespace std;

#define INVALID_CONTACT "-1-1-1-1"

class Contact
{
    string name;
public:
    Contact(string name);
    string getName();
    bool isValid();

};

#endif // CONTACT_H
