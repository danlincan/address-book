#ifndef ADDRESSBOOKCONTAINER_H
#define ADDRESSBOOKCONTAINER_H

#include "Contact.h"
#include <string>

using namespace std;

class AddressBookContainer
{
public:
    virtual void Insert(std::string, Contact) = 0;
    virtual Contact Search(std::string) = 0;
    virtual Contact Delete(std::string) = 0;
    virtual void Display() = 0;
};

#endif // ADDRESSBOOKCONTAINER_H
