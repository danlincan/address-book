#!/bin/bash

NAME="${1}_abs${2}_ns${3}"

TITLE="Benchmark Vector"
if [ "$1" = "t" ]; then
	TITLE="Benchmark Radix Tree"
fi

gnuplot -p << EOF
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set title "$TITLE"
set xlabel "Address Book size"
set ylabel "Time (us)"
plot	"$NAME" using 1:4 title 'Search time ($3 searches)' with linespoints \

pause -1

EOF
