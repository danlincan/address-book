#include "Benchmark.h"

static timespec diff(timespec start, timespec end)
{
    timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

void Benchmark::Start()
{
    clock_gettime(CLOCK_REALTIME, &start);
}

void Benchmark::Stop()
{
    clock_gettime(CLOCK_REALTIME, &end);
}

void Benchmark::printResult(std::ostream *out)
{
    timeDiff = diff(start, end);
    long time = timeDiff.tv_sec * 1000000 + timeDiff.tv_nsec / 1000;

    *out << time << " ";
}

