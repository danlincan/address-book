#include "AddressBookRadixTreeContainer.h"

string AddressBookRadixTreeContainer::findCommonPrefix(string key, string match)
{
    string prefix = "";
    /* find out which one has the smallest size */
    int length = min(key.length(), match.length());

    for(int i = 0; i < length; ++i)
    {
        if(key[i] == match[i])
            prefix += key[i];
    }
    return prefix;
}

void AddressBookRadixTreeContainer::Insert(string key, Contact contact)
{
    AddressBookRadixTreeContainer *node = this;
    /* nr of characters that have been processed from the key*/

    while( node != NULL )
    {
        /* iterate through the current node's childrens */
        string prefix = "";
        for(size_t i = 0; i < node->children.size(); ++i)
        {
            prefix = findCommonPrefix(key, node->children[i].getKey());

            if( !prefix.empty() )
            {
                node = &node->children[i];
                /*  if it's the whole word add it to the current node
                    and finish insertion
                */
                if( prefix.length() == key.length() )
                {
                    /* update node data */
                    node->data = contact;
                    /* exit from while */
                    node = NULL;
                }
                /* update nr of characters processed in the key */
                key = key.substr(prefix.length());

                /* advance further in the tree */
                break;
            }
        }
        /*
            if no matching prefix
            add the data in a new children node
        */
        if( prefix.empty() )
        {
            AddressBookRadixTreeContainer child;
            child.setKey(key);
            child.setData(contact);
            node->children.push_back(child);
            /* and exit */
            break;
        }
    }
}

Contact AddressBookRadixTreeContainer::Delete(string)
{

}

Contact AddressBookRadixTreeContainer::Search(string key)
{
    AddressBookRadixTreeContainer *node = this;
    string currentKey = key;

    while( node != NULL )
    {
        //cout << "node [" << node->getKey() << "]" << endl;
        /* found match */
        if( node->key == key )
            return node->data;

        string prefix = "";
        for(size_t i = 0; i < node->children.size(); ++i)
        {
            prefix = findCommonPrefix(currentKey, node->children[i].getKey());
            if( prefix.empty() )
                continue;
            /* found match */
            if( prefix.length() == currentKey.length() )
                return node->children[i].getData();
            node = &node->children[i];
            currentKey = currentKey.substr(prefix.length());
        }
        /* no match */
        if( prefix.empty() )
            break;
    }

    return Contact(INVALID_CONTACT);
}

void AddressBookRadixTreeContainer::Display()
{
    Display(this);
}

void AddressBookRadixTreeContainer::Display(AddressBookRadixTreeContainer *node, string padding)
{
    if( NULL == node )
        return;
    if( node->data.isValid() )
        cout << padding << node->data.getName() << endl;
    for(size_t i = 0; i < node->children.size(); ++i)
    {
        Display( &node->children[i], padding + "    " );
    }
}

void AddressBookRadixTreeContainer::setKey(string key)
{
    this->key = key;
}

string AddressBookRadixTreeContainer::getKey()
{
    return key;
}

void AddressBookRadixTreeContainer::setData(Contact data)
{
    this->data = data;
}

Contact AddressBookRadixTreeContainer::getData()
{
    return data;
}
