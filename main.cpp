#include <iostream>
#include "Trie.h"
#include "Benchmark.h"
#include "AddressBook.h"
#include "AddressBookVectorContainer.h"
#include "AddressBookRadixTreeContainer.h"

#include <vector>
#include <string>

#define ALPHABET_SIZE 52


/* defalt size */
int ab_size = 2048;
int nrOfSearches = 128;
int step = 32;

string container_type = "";
bool testVector = true;
bool testTrie = true;

using namespace std;

vector<Contact> entries;

/* generating random strings to insert in address book */
void init_entries(int address_book_size, int entry_size)
{
    for(int i = 0; i < address_book_size; ++i)
    {
        std::string str;
        for(int j = 0; j < entry_size; ++j)
        {
            str += (rand() % ALPHABET_SIZE + 'a');
        }
        entries.push_back(Contact(str));
    }
}

template <class T>
void simple_test(int address_book_size, int entry_size, int nrSearches, ostream *out)
{
    /* init benchmark class */
    Benchmark benchmark;

    /* init address book with vector container */
    T ct = T();
    AddressBook ab(&ct);

    /* testing insertion time */
    benchmark.Start();

    for(int i = 0; i < address_book_size; ++i)
    {
        ab.Insert(entries[i].getName(), entries[i]);
    }

    /* making sure that the vector container is sorted
      before benchmarking the search operation
      even if I don't do that it's still 4-5 times faster
      */
    AddressBookVectorContainer *container =
            dynamic_cast<AddressBookVectorContainer*>(&ct);
    if( container )
    {
        container->sort();
    }
    benchmark.Stop();
    benchmark.printResult(out);

    /* testing search time */
    benchmark.Start();
    for(int i = 0; i < nrSearches; ++i)
        ab.Search( entries[i].getName() );
    benchmark.Stop();
    benchmark.printResult(out);
}

template <class T>
void complex_test(int startSize, int endSize, int incSize, string outFile = "", int nrSearches = -1)
{
    int nrS;
    ostream *out;
    if(!outFile.empty())
    {
        out = new ofstream(outFile.c_str());
        //out->open(outFile.c_str(), fstream::out | fstream::app );
    } else
        out = &cout;

    for(; startSize <= endSize; startSize += incSize )
    {
        if( nrSearches == -1 )
            nrS = startSize;
        else
            nrS = nrSearches;

        if( nrS > startSize )
            nrS = startSize;

        *out << startSize << " " << nrS << " ";
        simple_test<T>(startSize, 128, nrS, out);

        *out << endl;
    }
    if(!outFile.empty())
        delete out;
}

void radix_tree_simple_test()
{
    AddressBookRadixTreeContainer ct;
    ct.Insert("test", Contact("test"));
    ct.Insert("testa", Contact("testa"));
    ct.Insert("testaa", Contact("testaa"));
    ct.Insert("testab", Contact("testab"));
    ct.Insert("testabc", Contact("testabc"));
    ct.Insert("testb", Contact("testb"));

    Contact c = ct.Search("taestaa");
    if( !c.isValid() )
        cout << "not ";
    cout <<"found" << endl;
    ct.Display();
}

void drawPlot(string type, char *argv[])
{
    string toExec = "./show_plot.sh " + type + " "
            + string(argv[1]) + " " + string(argv[2]);
    system(toExec.c_str());
}

int main(int argc, char *argv[])
{
    srand(12);
    if( argc !=3 && argc != 4 )
    {
        cout << "Usage:" << endl;
        cout << "\t" << argv[0] << "address_book_size nr_of_searches container_type[opt]" << endl;
        cout << endl << "containter_type = [vector|v], [trie|t], [both|b]" << endl;
        return EXIT_SUCCESS;
    }
    if( argc == 4)
    {
        container_type = string(argv[3]);
    }
    if( container_type == "vector" || container_type == "v" )
        testTrie = false;
    if( container_type == "trie" || container_type == "t" )
        testVector = false;

    ab_size = atoi(argv[1]);
    nrOfSearches = atoi(argv[2]);

    string outFile = "abs" + string(argv[1]) + "_ns" + string(argv[2]);
    init_entries(ab_size, 128);
    if(testVector)
    {
        complex_test<AddressBookVectorContainer>(128, ab_size, step, "v_" + outFile,nrOfSearches);
        drawPlot("v", argv);
    }

    if(testTrie)
    {
        complex_test<AddressBookRadixTreeContainer>(128, ab_size, step, "t_" + outFile, nrOfSearches);
        drawPlot("t", argv);
    }
    return EXIT_SUCCESS;
}

