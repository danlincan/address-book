#ifndef ADDRESSBOOKRADIXTREECONTAINER_H
#define ADDRESSBOOKRADIXTREECONTAINER_H

#include "AddressBookContainer.h"
#include "Contact.h"

#include <vector>
#include <algorithm>

class AddressBookRadixTreeContainer : public AddressBookContainer
{
    string key;
    Contact data;
    vector<AddressBookRadixTreeContainer> children;
    string findCommonPrefix(string, string);
public:
    AddressBookRadixTreeContainer() : key(""), data(Contact(INVALID_CONTACT)) {};
    void Insert(string, Contact);
    Contact Delete(string);
    Contact Search(string);
    void Display();
    void Display(AddressBookRadixTreeContainer *node, string padding = "");
    void setKey(string);
    string getKey();
    void setData(Contact data);
    Contact getData();
};

#endif // ADDRESSBOOKRADIXTREECONTAINER_H
