#include "AddressBookVectorContainer.h"

static bool compare(Contact lhs, Contact rhs)
{
    return lhs.getName() < rhs.getName();
}

std::vector<Contact>::iterator
AddressBookVectorContainer::SearchIterator(std::string key)
{
    if(!sorted)
        sort();
    it = lower_bound(data.begin(), data.end(), Contact(key), compare);
    return it;
}

void AddressBookVectorContainer::Insert(std::string key, Contact c)
{
    sorted = false;
    data.push_back(c);
}

Contact AddressBookVectorContainer::Delete(std::string key)
{
    it = SearchIterator(key);
    if(it != data.end() && it->getName() == key)
        data.erase(it);
}

Contact AddressBookVectorContainer::Search(std::string key)
{
    it = SearchIterator(key);

    if( it != data.end() && it->getName() == key)
        return *it;
    return Contact(INVALID_CONTACT);
}

void AddressBookVectorContainer::Display()
{
    for(int i = 0; i < data.size(); ++i)
        cout << data[i].getName() << endl;
}

void AddressBookVectorContainer::sort()
{
    if( false == sorted )
    {
        std::sort(data.begin(), data.end(), compare);
        sorted = true;
    }
}
