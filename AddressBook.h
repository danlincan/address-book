#ifndef ADDRESSBOOK_H
#define ADDRESSBOOK_H

#include <string>
#include "Contact.h"
#include "AddressBookContainer.h"

class AddressBook
{
    AddressBookContainer *c;
public:
    AddressBook(AddressBookContainer *c);
    void Insert(std::string, Contact);
    Contact Search(std::string);
    Contact Delete(std::string);
    void Display();
};

#endif // ADDRESSBOOK_H
