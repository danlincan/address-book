#ifndef ADDRESSBOOKVECTORCONTAINER_H
#define ADDRESSBOOKVECTORCONTAINER_H

#include "AddressBookContainer.h"
#include "Contact.h"

#include <string>
#include <vector>
#include <algorithm>

class AddressBookVectorContainer : public AddressBookContainer
{
    std::vector<Contact> data;
    std::vector<Contact>::iterator it;
    /* we sort only when we search something */
    bool sorted;

    std::vector<Contact>::iterator SearchIterator(std::string);

public:
    AddressBookVectorContainer() : sorted(true) {}
    void Insert(std::string, Contact);
    Contact Search(std::string);
    Contact Delete(std::string);
    void Display();
    void sort();
};

#endif // ADDRESSBOOKVECTORCONTAINER_H
